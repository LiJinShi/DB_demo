//
//  TimeInterval.m
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import "TimeInterval.h"

@implementation TimeInterval

+ (long long)currentTimeInterval
{
    NSDate *newDate = [NSDate new];
    long long tempD = [newDate timeIntervalSince1970];
    return tempD;
}

@end
