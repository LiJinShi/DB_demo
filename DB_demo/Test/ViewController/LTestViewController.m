//
//  LTestViewController.m
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import "LTestViewController.h"
#import "LTestView.h"
#import "LHomeViewModel.h"
#import "LCityCell.h"
#import "MJExtension.h"

#define WS(weakSelf) __weak typeof(self) weakSelf = self;

@interface LTestViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) LTestView *LTestView;
@property (nonatomic, strong) LHomeViewModel *homeViewModel;

@end

@implementation LTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.homeViewModel = [LHomeViewModel new];
    self.LTestView.collectionView.delegate = self;
    self.LTestView.collectionView.dataSource = self;
    [self.LTestView.collectionView registerClass:[LCityCell class] forCellWithReuseIdentifier:NSStringFromClass([LCityCell class])];
    
    [self reloadHistoryData];
}

- (LTestView *)LTestView
{
    if (!_LTestView) {
        _LTestView = [[LTestView alloc] initWithFrame:self.view.bounds];
        [self.view addSubview:_LTestView];
    }
    return _LTestView;
}

- (void)reloadHistoryData
{
    NSMutableArray *historyArray = [self.homeViewModel queryHistoryData];
    [self.LTestView.historyView reloadDataWithArray:historyArray];
}

#pragma mark UICollectionViewDelegate, UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.homeViewModel.testDataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LCityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LCityCell class]) forIndexPath:indexPath];
    [cell reloadDataWithVM:self.homeViewModel index:indexPath.item];
    
    WS(weakSelf);
    cell.btnBlock = ^(LCityModel *cityM) {
        NSLog(@"点击了%@", cityM.cityName);
        [weakSelf.homeViewModel insertData:cityM];
        
        [weakSelf reloadHistoryData];
    };
    return cell;
}

@end
