//
//  LCityModel.m
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import "LCityModel.h"

@implementation LCityModel

+ (NSString *)primaryKey
{
    return @"cityId";
}

@end
