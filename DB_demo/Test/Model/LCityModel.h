//
//  LCityModel.h
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface LCityModel : RLMObject

@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, assign) NSInteger cityId;

// 数据更新时间
@property (nonatomic, strong) NSDate *updateDate;

@end
