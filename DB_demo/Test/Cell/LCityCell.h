//
//  LCityCell.h
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LHomeViewModel.h"

@interface LCityCell : UICollectionViewCell

// 按钮点击回调
@property (nonatomic, strong) void(^btnBlock)(LCityModel *cityM);

// 设置数据
- (void)reloadDataWithVM:(LHomeViewModel *)homeViewModel index:(NSInteger)index;

@end
