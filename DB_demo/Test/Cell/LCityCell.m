//
//  LCityCell.m
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import "LCityCell.h"
#import "Masonry.h"
#import <Realm/Realm.h>

@interface LCityCell()

@property (nonatomic, strong) UIButton *cityBtn;

// 保存当前cell 对应的 model
@property (nonatomic, strong) LCityModel *cityM;

@end

@implementation LCityCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self layout];
    }
    return self;
}

- (void)layout
{
    [self.cityBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
}

- (UIButton *)cityBtn
{
    if (!_cityBtn) {
        _cityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _cityBtn.backgroundColor = [UIColor brownColor];
        [_cityBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [_cityBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.contentView addSubview:_cityBtn];
        [_cityBtn addTarget:self action:@selector(cityBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cityBtn;
}

#pragma mark 按钮点击
- (void)cityBtnClick
{
    
    
    
    
    if (self.btnBlock) {
        self.btnBlock(self.cityM);
    }
}

#pragma mark 设置数据
- (void)reloadDataWithVM:(LHomeViewModel *)homeViewModel index:(NSInteger)index
{
    self.cityM = homeViewModel.testDataArray[index];
    NSString *title = homeViewModel.testDataArray[index].cityName;
    [self.cityBtn setTitle:title forState:UIControlStateNormal];
}

@end
