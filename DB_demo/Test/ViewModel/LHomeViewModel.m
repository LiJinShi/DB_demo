//
//  LHomeViewModel.m
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import "LHomeViewModel.h"
#import "MJExtension.h"
#import "LCityModel.h"

@interface LHomeViewModel()

@end

@implementation LHomeViewModel

- (NSMutableArray *)testDataArray
{
    if (!_testDataArray) {
        NSArray *array = @[@{@"cityId": @1, @"cityName": @"北京"},
                           @{@"cityId": @2, @"cityName": @"上海"},
                           @{@"cityId": @3, @"cityName": @"广州"},
                           @{@"cityId": @4, @"cityName": @"深圳"},
                           @{@"cityId": @5, @"cityName": @"杭州"},
                           @{@"cityId": @6, @"cityName": @"南昌"},
                           @{@"cityId": @7, @"cityName": @"武汉"},
                           @{@"cityId": @8, @"cityName": @"天津"},
                           @{@"cityId": @9, @"cityName": @"重庆"},
                           @{@"cityId": @10, @"cityName": @"厦门"}];
        NSMutableArray *tempArray = [LCityModel mj_objectArrayWithKeyValuesArray:array];
        _testDataArray = [[NSMutableArray alloc] initWithArray:tempArray];
    }
    return _testDataArray;
}

// 插入一条数据
- (void)insertData:(LCityModel *)cityM
{
    cityM.updateDate = [NSDate new];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [LCityModel createOrUpdateInRealm:realm withValue:cityM];
    [realm commitWriteTransaction];
    
    // 判断如果大于4条就删除掉多于4条的最早的
    RLMResults<LCityModel *> *results = [[LCityModel allObjects] sortedResultsUsingKeyPath:@"updateDate" ascending:NO];
    if (results.count > 4) {
        for (int i = 4; i < results.count; i ++ ) {
            LCityModel *resultM = [results objectAtIndex:i];
            [realm transactionWithBlock:^{
                [realm deleteObject:resultM];
            }];
        }
    }
}

// 查询出4条最近的历史记录
- (NSMutableArray *)queryHistoryData
{
    NSMutableArray *historyArray = [NSMutableArray array];
    RLMResults *results = [[LCityModel allObjects] sortedResultsUsingKeyPath:@"updateDate" ascending:NO];
    for (LCityModel *tempM in results) {
        [historyArray addObject:tempM];
    }
    return historyArray;
}

@end
