//
//  LHomeViewModel.h
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LCityModel.h"

@interface LHomeViewModel : NSObject

@property (nonatomic, strong) NSMutableArray<LCityModel *> *testDataArray;

// 增加数据, 同时返回所有历史数据
- (void)insertData:(LCityModel *)cityM;
// 查询数据
- (NSMutableArray *)queryHistoryData;

@end
