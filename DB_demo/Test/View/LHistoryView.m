//
//  LHistoryView.m
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import "LHistoryView.h"
#import "Masonry.h"

@interface LHistoryView()

@property (nonatomic, strong) NSMutableArray *labelArray;

@end

@implementation LHistoryView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self layout];
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"LHistoryView----dealloc");
}

- (NSMutableArray *)labelArray
{
    if (!_labelArray) {
        _labelArray = [NSMutableArray array];
    }
    return _labelArray;
}

- (void)layout
{
    for (int i = 0; i < 4; i ++) {
        UILabel *label = [[UILabel alloc] init];
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor whiteColor];
        label.textColor = [UIColor blackColor];
        [self.labelArray addObject:label];
        [self addSubview:label];
    }
    
    // 水平方向等间距布局
    [self.labelArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal
                                 withFixedSpacing:20
                                      leadSpacing:12
                                      tailSpacing:12];
    // 竖直方向位置与高度
    [self.labelArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.height.mas_equalTo(40);
    }];
}

- (void)reloadDataWithArray:(NSMutableArray *)array
{
    for (int i = 0; i < array.count; i ++) {
        LCityModel *cityM = array[i];
        UILabel *label = self.labelArray[i];
        label.text = cityM.cityName;
    }
}


/**
 *  确定间距等间距布局
 *
 *  @param axisType     布局方向
 *  @param fixedSpacing 两个item之间的间距(最左面的item和左边, 最右边item和右边都不是这个)
 *  @param leadSpacing  第一个item到父视图边距
 *  @param tailSpacing  最后一个item到父视图边距
 */
//- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType
//                    withFixedSpacing:(CGFloat)fixedSpacing
//                         leadSpacing:(CGFloat)leadSpacing
//                         tailSpacing:(CGFloat)tailSpacing;


@end
