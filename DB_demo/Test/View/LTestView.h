//
//  LTestView.h
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LHistoryView.h"

@interface LTestView : UIView

// 展示历史数据
@property (nonatomic, strong) LHistoryView *historyView;
// 展示城市数据
@property (nonatomic, strong) UICollectionView *collectionView;


@end
