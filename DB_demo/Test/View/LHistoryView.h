//
//  LHistoryView.h
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LCityModel.h"

@interface LHistoryView : UIView

- (void)reloadDataWithArray:(NSMutableArray<LCityModel *> *)array;

@end
