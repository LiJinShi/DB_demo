//
//  LTestView.m
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import "LTestView.h"
#import "Masonry.h"

@interface LTestView()

@property (nonatomic, strong) UILabel *topL;
@property (nonatomic, strong) UILabel *bottomL;

@end

@implementation LTestView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self layout];
    }
    return self;
}

- (void)layout
{
    [self.historyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(100);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(80);
    }];
    [self.topL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.historyView.mas_top).offset(-10);
        make.left.mas_equalTo(self.historyView);
    }];
    
    [self.bottomL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.historyView);
        make.top.mas_equalTo(self.historyView.mas_bottom).offset(20);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.bottomL.mas_bottom).offset(10);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(300);
    }];
}

- (UILabel *)topL
{
    if (!_topL) {
        _topL = [[UILabel alloc] init];
        _topL.text = @"历史数据";
        [self addSubview:_topL];
    }
    return _topL;
}

- (UILabel *)bottomL
{
    if (!_bottomL) {
        _bottomL = [[UILabel alloc] init];
        _bottomL.text = @"城市数据";
        [self addSubview:_bottomL];
    }
    return _bottomL;
}

- (LHistoryView *)historyView
{
    if (!_historyView) {
        _historyView = [[LHistoryView alloc] init];
        _historyView.backgroundColor = [UIColor greenColor];
        [self addSubview:_historyView];
    }
    return _historyView;
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = 300;
        CGRect rect = CGRectMake(0, 0, width, height);
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 10;
        flowLayout.minimumInteritemSpacing = 10;
        flowLayout.itemSize = CGSizeMake(80, 30);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor grayColor];
        _collectionView.delaysContentTouches = NO;
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

@end
