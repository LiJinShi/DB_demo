//
//  AppDelegate.h
//  DB_demo
//
//  Created by lijinshi on 2018/3/16.
//  Copyright © 2018年 lijinshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

